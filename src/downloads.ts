import { MoviesHttp } from './hallus-http';
import { MoviesIcs } from './hallus-ics';

const icsMovies = new MoviesIcs(new MoviesHttp());

icsMovies.list().then(console.log);
