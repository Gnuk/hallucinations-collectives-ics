import * as ics from 'ics';
import { EventAttributes } from 'ics';

import { Movie, MoviesRepository } from './hallus';

const toEvent = ({ start, end, title, description, location }: Movie): EventAttributes => ({
  start: start.getTime(),
  end: end.getTime(),
  title,
  description,
  location,
});

const toOneIcs = (event: EventAttributes): Promise<string> =>
  new Promise((resolve, reject) => {
    ics.createEvent(event, (error, value) => {
      if (error) {
        reject(error);
      } else {
        resolve(value);
      }
    });
  });

const toAllIcs = (events: EventAttributes[]): Promise<string> =>
  new Promise((resolve, reject) => {
    ics.createEvents(events, (error, value) => {
      if (error) {
        reject(error);
      } else {
        resolve(value);
      }
    });
  });

export class MoviesIcs {
  constructor(private readonly repository: MoviesRepository) {}

  get(slug: string): Promise<string> {
    return this.repository.get(slug).then(toEvent).then(toOneIcs);
  }

  async list(): Promise<string> {
    return this.repository
      .list()
      .then((movies) => movies.map(toEvent))
      .then(toAllIcs);
  }
}
