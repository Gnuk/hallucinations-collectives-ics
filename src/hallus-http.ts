import { JSDOM } from 'jsdom';

import { Movie, Movies, MoviesRepository, Slug } from './hallus';

interface SeanceTexts {
  date: string;
  location: string;
}

const selectDom =
  (dom: JSDOM) =>
  (selector: string): string => {
    const result = dom.window.document.querySelector(selector)?.textContent;
    if (result === null || result === undefined) {
      throw new Error(`Impossible to find a text from the selector: ${selector}`);
    }
    return result;
  };

const linksDom = (dom: JSDOM): string[] => {
  return [...dom.window.document.querySelectorAll('#planning')].map((selector) => selector.closest('a')?.href) as string[];
};

const seanceTextsFrom = (seance: string): SeanceTexts => {
  const [, date, location] = seance.match(/.+séance :(.+[h][0-9]+)(.+)/i)?.map((part) => part.trim()) || [];

  if (date === undefined) {
    throw new Error(`Impossible to extract date from: ${seance}`);
  }

  if (location === undefined) {
    throw new Error(`Impossible to location location from: ${seance}`);
  }
  return { date, location };
};

const FRENCH_MONTHS: Record<string, string> = {
  janvier: '01',
  février: '02',
  mars: '03',
  avril: '04',
  mai: '05',
  juin: '06',
  juillet: '07',
  août: '08',
  septembre: '09',
  octobre: '10',
  novembre: '11',
  décembre: '12',
};

const toMonth = (text: string): string => {
  const month = FRENCH_MONTHS[text];

  if (month === undefined) {
    throw new Error(`The month ${text} is not available at the moment.`);
  }

  return month;
};

const toHours = (text: string): string =>
  text
    .toLowerCase()
    .split('h')
    .map((unit) => unit.padStart(2, '0'))
    .concat('00')
    .join(':');

const toDate = (text: string): Date => {
  const [, day, monthText, year, , hourText] = text.split(' ');

  return new Date(`${year}-${toMonth(monthText)}-${day.padStart(2, '0')}T${toHours(hourText)}`);
};

const durationTextFrom = (info: string) => {
  const duration = info
    .match(/.+Durée :(.+)min/)
    ?.at(1)
    ?.trim();

  if (duration === undefined) {
    throw new Error(`Impossible to extract duration from: ${info}`);
  }
  return +duration;
};

const addMinutes = (start: Date, duration: number) => new Date(start.getTime() + duration * 60000);

const filterSlug = (link: string): string[] => {
  const slug = link.match(/https:\/\/www.hallucinations-collectives.com\/movies\/(.+)\//)?.at(1);
  if (slug === undefined || slug === null) {
    return [];
  }
  return [slug];
};

const filterFulfilled = <T>(result: PromiseSettledResult<T>): T[] => {
  if (result.status === 'fulfilled') {
    return [result.value];
  }
  return [];
};

const toLocation = (text: string): string => {
  if (/marché gare(.+)Lyon/i.test(text)) {
    return 'Le Marché Gare, 4-6 Pl. Hubert Mounier, 69002 Lyon';
  }
  if (/Comoedia/i.test(text)) {
    return 'Comoedia, 13 Av. Berthelot, 69007 Lyon, France';
  }
  return text;
};

export class MoviesHttp implements MoviesRepository {
  async list(): Promise<Movies> {
    const slugs = await this.slugs();
    const settled = await Promise.allSettled(slugs.map((slug) => this.get(slug)));
    return settled.flatMap(filterFulfilled);
  }
  async get(slug: Slug): Promise<Movie> {
    const response = await fetch(`https://www.hallucinations-collectives.com/movies/${slug}/`);
    const body = await response.text();
    const select = selectDom(new JSDOM(body));
    const title = select('#title_movie');
    const description = select('#content #encart3 p');
    const seance = select('#seance');
    const { date, location } = seanceTextsFrom(seance);
    const start = toDate(date);
    const info = select('#content #encart:nth-child(2)');
    const duration = durationTextFrom(info);
    const end = addMinutes(start, duration);
    return {
      title,
      slug,
      description,
      start,
      end,
      location: toLocation(location),
    };
  }

  async slugs(): Promise<string[]> {
    const response = await fetch('https://www.hallucinations-collectives.com/edition-2024/calendar/');
    const body = await response.text();
    const links = linksDom(new JSDOM(body));

    return links.flatMap(filterSlug);
  }
}
