export type Slug = string;

export interface Movie {
  start: Date;
  end: Date;
  title: string;
  slug: Slug;
  description: string;
  location: string;
}

export type Movies = Movie[];

export interface MoviesRepository {
  get(slug: Slug): Promise<Movie>;
  list(): Promise<Movies>;
}
