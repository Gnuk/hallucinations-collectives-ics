import { describe, expect, it } from 'vitest';

import { MoviesHttp } from './hallus-http';

describe('HallusHttp', () => {
  it('should get themroc', async () => {
    const movies = new MoviesHttp();

    const movie = await movies.get('themroc');

    expect(movie.title).toBe('Themroc');
    expect(movie.description).toBe(
      'Michel Piccoli est Themroc : un ouvrier qui, dans le Paris du début des années 70, subit sa vie plutôt qu’il ne la vit. Après un déclic libérateur, Themroc prend conscience de l’absurdité du monde qui l’entoure et exprime sauvagement son individualité.'
    );
    expect(movie.start).toEqual(new Date('2024-03-29T14:30:00.000'));
    expect(movie.end).toEqual(new Date('2024-03-29T16:25:00.000'));
    expect(movie.location).toEqual('Comoedia, 13 Av. Berthelot, 69007 Lyon, France');
  });

  it('should get other-like-me', async () => {
    const movies = new MoviesHttp();

    const movie = await movies.get('other-like-me');

    expect(movie.title).toBe('Other, Like Me');
    expect(movie.description).toBe(
      "Hull, Angleterre, 1970. Un groupe d'inadaptés sociaux adopte de nouvelles identités et imagine, sous le nom de COUM Transmissions, des performances tournant essentiellement autour du sexe et de la violence. Dans le sillage de Genesis Breyer P-Orridge et Cosey Fanni Tutti, ils créent le scandale à chaque apparition, attirent de plus en plus l’attention et se verront qualifiés de “destructeurs de civilisation” par la presse britannique. C’est alors qu’ils décident de se tourner vers la musique et fondent le groupe Throbbing Gristle, qui posera les bases du courant dit “industriel”."
    );
    expect(movie.start).toEqual(new Date('2024-03-21T20:00:00.000'));
    expect(movie.end).toEqual(new Date('2024-03-21T21:22:00.000'));
    expect(movie.location).toEqual('Le Marché Gare, 4-6 Pl. Hubert Mounier, 69002 Lyon');
  });

  it('should list movies', async () => {
    const movies = new MoviesHttp();

    const list = await movies.list();

    const titles = list.map((movie) => movie.title);
    expect(titles).toContain('Concrete Utopia');
    expect(titles).toContain('Une journée bien remplie');
  });

  it('should list slugs', async () => {
    const movies = new MoviesHttp();

    const slugs = await movies.slugs();

    expect(slugs).toContain('steak');
    expect(slugs).toContain('concrete-utopia');
  });
});
