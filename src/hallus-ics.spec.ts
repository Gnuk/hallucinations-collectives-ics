import { describe, expect, it } from 'vitest';

import { Movie, Movies, MoviesRepository } from './hallus';
import { MoviesIcs } from './hallus-ics';

const movie: Movie = {
  title: 'Title',
  slug: 'slug',
  start: new Date('2024-01-01T10:00:00.000Z'),
  end: new Date('2024-01-01T12:30:00.000Z'),
  description: 'Description',
  location: 'Location',
};

const secondMovie: Movie = {
  title: 'Second',
  slug: 'second',
  start: new Date('2024-01-02T20:00:00.000Z'),
  end: new Date('2024-01-02T22:30:00.000Z'),
  description: 'Second description',
  location: 'Second location',
};

const movies: Movies = [movie, secondMovie];

class MoviesInMemory implements MoviesRepository {
  async get(): Promise<Movie> {
    return movie;
  }

  async list(): Promise<Movies> {
    return movies;
  }
}

const expectFirst = (movie: string) => {
  expect(movie).toContain('DTSTART:20240101T100000Z');
  expect(movie).toContain('DTEND:20240101T123000Z');
  expect(movie).toContain('SUMMARY:Title');
  expect(movie).toContain('DESCRIPTION:Description');
  expect(movie).toContain('LOCATION:Location');
};

describe('HallusICS', () => {
  it('should get', async () => {
    const movies = new MoviesIcs(new MoviesInMemory());

    const movie = await movies.get('slug');

    expectFirst(movie);
  });
  it('should list', async () => {
    const movies = new MoviesIcs(new MoviesInMemory());

    const movie = await movies.list();

    expectFirst(movie);
    expect(movie).toContain('DTSTART:20240102T200000Z');
    expect(movie).toContain('DTEND:20240102T223000Z');
    expect(movie).toContain('SUMMARY:Second');
    expect(movie).toContain('DESCRIPTION:Second description');
    expect(movie).toContain('LOCATION:Second location');
  });
});
