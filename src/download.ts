import { MoviesHttp } from './hallus-http';
import { MoviesIcs } from './hallus-ics';

const slug = process.argv[2];

const icsMovies = new MoviesIcs(new MoviesHttp());

icsMovies.get(slug).then(console.log);
